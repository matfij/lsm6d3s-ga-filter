#include "SparkFunLSM6DS3.h"
#include "Wire.h"


LSM6DS3 imu;
int clockFrequency = 400000;

double interval = 0, preInterval = 0;
float gax = 0, gay = 0;
float sgx = 0, bx = 0, sgy = 0, by = 0;
float tb = 0, ax = 0, ay = 0, az = 0, dx, dy, dz;

// algorithm parameters
int calibrationLength = 15000;
float lowCutoff = 0.0125;
float highCutoff = 1.00;


void setup() 
{
  Serial.begin(9600);
  Wire.setClock(clockFrequency);
  delay(1000);
  imu.begin();

  // gyro calibration
  for (int i = 0; i < calibrationLength; i++)
  {
     float gx = imu.readFloatGyroX();
     sgx += gx;
  }
  bx = sgx/calibrationLength;

  for (int i = 0; i < calibrationLength; i++)
  {
     float gy = imu.readFloatGyroY();
     sgy += gy;
  }
  by = sgy/calibrationLength;

  preInterval = millis();
}


void loop()
{
  interval = (millis() - preInterval) * 0.001;
  preInterval = millis();
  
  // accel: map acceleration to angle + low-pass filter + averaging
  for (int i = 1; i < 10; i++)
  {
    tb = imu.readFloatAccelX();
    if (abs(tb - dx) > highCutoff)  ax += 0;
    else ax += tb;
    dx = tb;
    tb = imu.readFloatAccelY();
    if (abs(tb - dy) > highCutoff)  ay += 0;
    else ay += tb;
    dy = tb;
    tb = imu.readFloatAccelZ();
    if (abs(tb - dz) > highCutoff)  az += 0;
    else az += tb;
    dz = tb;
  }
  ax = ax/10;
  ay = ay/10;
  az = az/10;
  
  ax = atan2(ax, sqrt(ay * ay) + (az * az)) * 180/PI;
  ay = atan2(ay, sqrt(ax * ax) + (az * az)) * 180/PI;

  // gyro: map velocity to angle + high-pass filter
  float gx = imu.readFloatGyroX() - bx;
  float gy = imu.readFloatGyroY() - by;
  
  if (abs(gx * interval) > lowCutoff)
  {
    gax += gx * interval;
  }
  if (abs(gy * interval) > lowCutoff)
  {
    gay += gy * interval;
  }
  
  // complimentary filter
  float x = 0.975*gax + 0.025*ax;
  float y = 0.975*gay + 0.025*ay;
  
  Serial.println(ax);
  
  // mediana filter

  
  
}
